Name:           spice-streaming-agent
Version:        @VERSION@
Release:        1%{?dist}
Summary:        SPICE streaming agent
Group:          Applications/System
License:        ASL 2.0
URL:            https://www.redhat.com
Source0:        %{name}-%{version}.tar.xz
BuildRequires:  spice-protocol >= @SPICE_PROTOCOL_MIN_VER@
BuildRequires:  libX11-devel libXfixes-devel
BuildRequires:  libjpeg-turbo-devel
BuildRequires:  catch-devel
BuildRequires:  pkgconfig(udev)
BuildRequires:  libdrm-devel
BuildRequires:  libXrandr-devel
BuildRequires:  gcc-c++
BuildRequires:  diffutils
BuildRequires:  meson >= 0.49
BuildRequires:  gstreamer1-devel
BuildRequires:  gstreamer1-plugins-base-devel
BuildRequires:  pixman-devel
BuildRequires:  glib2-devel
BuildRequires:  opus-devel
BuildRequires:  openssl-devel
%if 0%{?rhel} && 0%{?rhel} <= 7
BuildRequires:  python36
BuildRequires:  python36-six
BuildRequires:  python36-pyparsing
%else
BuildRequires:  python3
BuildRequires:  python3-six
BuildRequires:  python3-pyparsing
%endif
# we need /usr/sbin/semanage program which is available on different
# packages depending on distribution
Requires(post): /usr/sbin/semanage
Requires(postun): /usr/sbin/semanage

%description
An agent, running on a guest, sending video streams of the X display to a
remote client (over SPICE).

%package devel
Requires: spice-protocol >= @SPICE_PROTOCOL_MIN_VER@
Requires: pkgconfig
Requires: libX11-devel
Summary:  SPICE streaming agent development files

%description devel
This package contains necessary header files to build SPICE streaming
agent plugins.

%prep
%setup -q

%build
%meson -Dunittests=enabled -Dudevrulesdir=%{_udevrulesdir}
%meson_build

%check
%meson_test

%install
%meson_install

%post
semanage fcontext -a -t xserver_exec_t %{_bindir}/spice-streaming-agent 2>/dev/null || :
restorecon %{_bindir}/spice-streaming-agent || :

%postun
if [ $1 -eq 0 ] ; then  # final removal
semanage fcontext -d -t xserver_exec_t %{_bindir}/spice-streaming-agent 2>/dev/null || :
fi


%files
%doc COPYING NEWS README
%{_udevrulesdir}/90-spice-guest-streaming.rules
%{_bindir}/spice-streaming-agent
%{_sysconfdir}/xdg/autostart/spice-streaming.desktop
%{_datadir}/gdm/greeter/autostart/spice-streaming.desktop
%{_mandir}/man1/spice-streaming-agent.1.gz
%{_libdir}/%{name}/plugins/gst-plugin.so

%files devel
%defattr(-,root,root,-)
%{_includedir}
%{_libdir}/pkgconfig

%changelog
* Thu Sep 19 2019 Uri Lublin <uril@redhat.com> - 0.3-1
- Update to 0.3 release

* Thu May 31 2018 Uri Lublin <uril@redhat.com> - 0.2-1
- First release

* Wed Aug 16 2017 Frediano Ziglio <fziglio@redhat.com> - 0.1-1
- Initial package (pre-release)
